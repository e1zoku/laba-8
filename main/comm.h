#include "constans.h"

#ifndef COMM_H
#define COMM_H

struct date {
    int day;
    int month;
    int year;
};

struct times {
    int sec;
    int min;
    int hour;
};

struct comm {
    date date;
    times start;
    times last;
    double value;
    char type[MAX_STRING_SIZE];
    char number[MAX_STRING_SIZE];
};
#endif