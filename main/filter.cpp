#include "filter.h"
#include <string.h>

void typefilt(comm* array[], int size) {
	for (int i = 0; i < size; i++) {
		if (strcmp(array[i]->type, "мобильный") == 0) {
			output(array[i]);
		}
	}
}

void monthfilt(comm* array[], int size) {
	for (int i = 0; i < size; i++) {
		if (array[i]->date.month == 11 && array[i]->date.year == 2021) {
			output(array[i]);
		}
	}
}